# es6/express/postgres

This project includes a seed file and basic postgres database containing a users table with columns `userId` and `email`.

## setup

``` bash
# 1. create postgres database

# 2. install sequelize-cli globally
yarn add global sequelize-cli

# 3. make a '.env' file in your project root and fill with:
DB=dbname
DB_HOST=localhost (detault)
DB_PORT=5432 (default)
DB_USER=user

# 3. if first time running project, create/migrate/seed db:
sequelize db:create && sequelize db:migrate && sequelize db:seed:all

# 4. run "yarn start" to install + run dev mode
yarn start
```

## commands

``` bash
# reinstall + run yarn dev
yarn start

# dev mode
yarn dev

# build for production and run server
yarn prod
```

## Todo
- need to set DATABASE_URL in prod on heroku for use by config/config.js