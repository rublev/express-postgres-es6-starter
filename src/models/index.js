import sequelize from './sequelize';

export default {
  sequelize,
  User: sequelize.import('./user'),
}
