import Sequelize from 'sequelize';

import dotenv from 'dotenv'
dotenv.config()

const DB = process.env.DB;
const DB_HOST = process.env.DB_HOST;
const DB_PORT = process.env.DB_PORT;
const DB_USER = process.env.DB_USER;

let sequelize

if (process.env.NODE_ENV === 'production') {
  sequelize = new Sequelize(process.env.DATABASE_URL, {
    dialect: 'postgres',
    protocol: 'postgres',
    dialectOptions: {
      ssl: true,
    },
    logging: false,
  })
} else {
  sequelize = new Sequelize(`postgres://${DB_USER}@${DB_HOST}:${DB_PORT}/${DB}`, {
    dialect: 'postgres',
    logging: false,
  })
}

export default sequelize;