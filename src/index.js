import dotenv from 'dotenv'
dotenv.config()

import http from 'http';
import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import api from './api';
import db from './models/';

let app = express();
app.server = http.createServer(app);

// logger
app.use(morgan('dev'));

app.use(bodyParser.json({
	limit : '100kb'
}));

app.use('/api', api(db));

app.server.listen(process.env.PORT || 8080, () => {
	console.log(`Started on port ${app.server.address().port}`);
});

export default app;
