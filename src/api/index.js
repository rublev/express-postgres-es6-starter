import { Router } from 'express';

import { Op } from 'Sequelize'

export default (db) => {
	let api = Router();

	api.get('/', (req, res) => {
		res.json({ 'test': 'test' });
	});

	api.get('/test', (req, res) => {
		// this test update works

		// db.User.update({
		// 	email: 'test',
		// }, {
		// 	where: {
		// 		email: {
		// 			[Op.eq]: '321'
		// 		}
		// 	}
		// })

		res.sendStatus(200)
	})

	return api;
}