require('dotenv').config()

const DB = process.env.DB;
const DB_HOST = process.env.DB_HOST;
const DB_USER = process.env.DB_USER;

module.exports = {
  'development': {
    'username': DB_USER,
    'password': null,
    'database': DB,
    'host': DB_HOST,
    'dialect': 'postgres'
  },
  'production': {
    'use_env_variable': 'DATABASE_URL',
    'dialect': 'postgres'
  }
}